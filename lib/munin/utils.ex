defmodule Munin.Utils do
  @moduledoc false

  @doc """
  Переводит имя модуля из CamelCase в snake_case. Не игнорирует модуль эликсира

  ## Examples

      iex> Munin.Utils.mname_to_dcase(Munin.Utils)
      "elixir._munin._utils"

  """
  @spec mname_to_dcase(atom | binary) :: binary
  def mname_to_dcase(mname) when is_atom(mname), do: mname |> Atom.to_string() |> mname_to_dcase()

  def mname_to_dcase(mname) when is_binary(mname) do
    mname
    |> String.split(~r/(?=[A-Z])/)
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(&String.downcase/1)
    |> Enum.join("_")
  end

  def do_call(call_to, params) do
    call_to |> :global.whereis_name() |> call(call_to, params)
  rescue
    reason -> do_error(call_to, reason, params, "call")
  catch
    reason -> do_error(call_to, inspect(reason), params, "call")
    _any, _reason ->
      do_error(call_to, "node down", params, "call")
  end

  def do_cast(call_to, params) do
    call_to |> :global.whereis_name() |> cast(call_to, params)
  rescue
    reason -> do_error(call_to, reason, params, "cast")
  catch
    reason -> do_error(call_to, inspect(reason), params, "cast")
    _any, _reason ->
      do_error(call_to, "node down", params, "cast")
  end

  def call(:undefined, call_to, params),
    do: do_error(call_to, "unavaliable service", params, "call")

  def call(pid, _call_to, params), do: pid |> GenServer.call(params, :infinity)

  def cast(:undefined, call_to, params),
    do: do_error(call_to, "unavaliable service", params, "cast")

  def cast(pid, _call_to, params), do: pid |> GenServer.cast(params)

  def do_error(call_to, reason, params, type) do
    {:unhandled_error, %{reason: reason, params: params, service: call_to, request_type: type}}
  end

  def write_file(func_type, params, service_name, app) do
    spawn(fn ->
      if is_important?(service_name, func_name(params), app) do
        module = get_munin_module(service_name, app)
        data = inspect(%{func_type: func_type, error: params})

        params = [
          data,
          "#{apply(module, :logs_path, [])}/[#{Date.utc_today()}]#{service_name}",
          "txt"
        ]

        apply(module, :write_file, params)
      end
    end)
  end

  def do_send(list, mod, app) when is_list(list),
    do:
      Enum.each(list, fn filename ->
        get_munin_module(mod, app)
        |> apply(:read_logs_and_remove, [filename])
        |> Enum.each(&do_send/1)
      end)

  def do_send(%{
        error: %{params: params, service: service, request_type: request_type},
        func_type: func_type
      }) do
    case func_type do
      "call" -> :gen_statem.call(String.to_atom(service), {:"#{request_type}", params})
      "cast" -> :gen_statem.cast(String.to_atom(service), {:"#{request_type}", params})
      _ -> "invalid param or function is not impotant"
    end
  end

  def is_important?(current_mod, func_name, app) do
    get_munin_module(current_mod, app)
    |> apply(:is_important?, [func_name])
  end

  def get_munin_module(current_mod, app) do
    {:ok, list} = :application.get_key(app, :modules)

    list
    |> Enum.reduce({nil, 0}, fn mod_name, {_word, distance} = acc ->
      mname = Module.split(mod_name) |> List.last()
      new_dist =
        String.jaro_distance(mname_to_dcase(mname), to_string_mod(current_mod))

      if new_dist > distance, do: {mod_name, new_dist}, else: acc
    end)
    |> elem(0)
  rescue
    _e -> nil
  end

  def to_string_mod(mod) when is_atom(mod), do: Atom.to_string(mod)
  def to_string_mod(mod) when is_binary(mod), do: mod

  @spec func_name(atom | tuple | %{:params => tuple, optional(any) => any}) :: atom
  def func_name(%{params: params}) when is_tuple(params), do: func_name(params)
  def func_name(%{params: params}) when is_atom(params), do: func_name(params)
  def func_name(params) when is_tuple(params), do: elem(params, 0)
  def func_name(params) when is_atom(params), do: params
end
