defmodule Munin do
  @moduledoc ~S"""
  Munin is a library for safely communication between modules via calls (stores data when service could not be called)
  For now it contains adapters:

    *  Munin.FileWritter: it will create fsm that will observe the connection state. If the connection is broken, then it starts writing according to the algorithm and important functions list defined in the module. Every 10 seconds, the service will check for a connection, if it does, the service will execute all the commands written to the file and continue working with the service

  The library itself consists of 3 main components:
    * Munin.FileWritter
    * Supervisor
    * Munin wrapper (macro and behaviour)

  ## Wrapper

    `Munin` is a wrapper around fsm. We can define it as follows:

      defmodule MyApp.CommunicatigWith do
        use Munin,
          adapter: Munin.FileWritter,
          otp_app: :my_app,
          log_path: "priv/unhandled_logs",
          service_name: "communicating_with"

        def important_list, do: ~w(factorial)a
        def read_file(filename), do: Filer.read_file(filename)
        def count_log(filename), do: Filer.count_logs(filename)
        def read_file_and_remove(filename), do: Filer.read_file_and_remove(filename)
        def write_file(data, filename, ext), do: Filer.write_file(data, filename, ext)
      end

    `Munin.Classic.Behaviour` - should suggest which functions should be predefined to implement the work of this or that type of wrapper
    It is important that the module name is at least close to the service name, it is better if it matches

  After all you just need to add to childrens in your application (supervisor) to start wrapper as supervisor

      def start(_type, _args) do
        children = [
          MyApp.CommunicatigWith
        ]
        opts = [strategy: :one_for_one, name: MyApp.Supervisor]
        Supervisor.start_link(children, opts)
      end

  And also add `:munin` to extra_applications

      def application do
        [
          ...,
          extra_applications: [..., :munin]
        ]
      end

  ## Using

  After the supervisor starts, the module will provide 2 main functions:
  * call
  * cast

  Let's see an example:

      iex> MyApp.CommunicatigWith.call({:factorial, 10})
      3628800
      iex> MyApp.CommunicatigWith.cast({:save_value, 10})
      :ok

  > NOTE: call is synchronous. By default it has no timeout.
  > cast is async.

  And some other functions like:
  * module_call - call to machine directly
  * module_name
  * app_name
  ...etc

  Example:

      iex> MyApp.CommunicatigWith.module_call(:service_name)
      :communicating_with
      iex> MyApp.CommunicatigWith.module_call(:list_log_files)
      [...]

  """
  @typedoc false
  @type module_vars :: [{:adapter, module()},{:log_path, binary}, {:service_name, binary}, {:otp_app, atom}]

  @spec __using__(module_vars) :: {:__block__, [], list()}
  defmacro __using__(adapter: adapter, otp_app: app_name, log_path: log_path, service_name: mname)
           when is_binary(mname) and is_binary(log_path) do
    quote do
      @behaviour unquote(adapter).Behaviour

      def logs_path, do: unquote(File.cwd! <> "/#{log_path}")
      def module_name, do: unquote(String.to_atom(mname))
      def app_name, do: unquote(app_name)

      def start(:normal, options), do: start_link(options)
      def start_link(opts) do
        File.mkdir_p(logs_path())
        global_node_addr  = Keyword.get(opts, :global_node_addr)
        Munin.FSM.Supervisor.create(unquote(adapter), unquote(String.to_atom(mname)), unquote(app_name), global_node_addr)
      end
      def stop(global_node_addr), do: Munin.FSM.Supervisor.stop(unquote(String.to_atom(mname)), unquote(app_name), global_node_addr)

      def child_spec(opts) do
        %{
          id: __MODULE__,
          start: {__MODULE__, :start_link, [opts]},
          type: :worker,
          restart: :permanent
        }
      end

      def read_logs(filename), do: unquote(adapter).Behaviour.read_logs(__MODULE__, filename)

      def read_logs_and_remove(filename),
        do: unquote(adapter).Behaviour.read_logs_and_remove(__MODULE__, filename)

      def count_logs(filename), do: unquote(adapter).Behaviour.count_logs(__MODULE__, filename)
      def list_logs, do: unquote(adapter).Behaviour.list_logs(__MODULE__)

      def module_call(params), do: :gen_statem.call(unquote(String.to_atom(mname)), params)
      def call(params), do: :gen_statem.call(unquote(String.to_atom(mname)), {:call, params})
      def cast(params), do: :gen_statem.call(unquote(String.to_atom(mname)), {:cast, params})

      def is_important?(function) when is_tuple(function), do: function in important_list()
      def is_important?(function) when is_atom(function), do: function in important_list()
    end
  end

  @doc """
  The start function launches the necessary modules for work
  """
  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_, opts), do: start_link(opts)
  @doc false
  @spec start_link(any) :: {:error, any} | {:ok, pid}
  def start_link(opts), do: Munin.Application.start(:normal, opts)

  def fetch_machine_addr do
    :inet.getif()
    |> elem(1)
    |> Enum.filter(& elem(&1, 2) == {255, 255, 255, 0})
    |> Enum.map(fn {ip, _br_addr, _mask} ->
      ip
      |> Tuple.to_list()
      |> Enum.join(".")
    end)
    |> List.first
  end
end
