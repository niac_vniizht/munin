defmodule Munin.FileWritter do
  @moduledoc """
  The machine that controls access to external services.
  Should the connection be broken, should redirect the event stream to a file.

  The machine starts up through its supervisor.
  Uses the interface of `gen_statem`
  """
  @behaviour :gen_statem
  @retry_timeout 10_000
  @send_timeout 10_000
  require Logger
  alias Munin.Utils

  @typedoc false
  @type action :: any()
  @typedoc false
  @type prev_state :: any()
  @typedoc false
  @type event :: :gen_statem.event_type()
  @typedoc false
  @type fsm_data :: any
  @type start_params :: {atom(), atom(), atom()}
  @type init_params :: map()

  @spec start_link({:via, atom(), {atom(), start_params()}}) :: :gen_statem.start_ret()
  @spec start_link(list(:gen_statem.start_opt())) :: :gen_statem.start_ret()
  @spec start({:via, atom(), {atom(), start_params()}}) :: :gen_statem.start_ret()
  @spec stop(atom) :: :ok
  @spec init(start_params()) :: {:ok, :connected, init_params()}
  @spec callback_mode :: :handle_event_function
  @spec handle_event(event(), action(), prev_state(), fsm_data()) ::
          :gen_statem.handle_event_result()
  @spec child_spec(any) :: %{
          id: __MODULE__,
          restart: :permanent,
          start: {__MODULE__, :start_link, any()},
          type: :worker
        }

  @doc false
  def start_link(name), do: start(name)

  @doc false
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent
    }
  end

  @doc false
  def start({:via, Registry, {_registry_name, {service_name, app_name, global_node_addr}}}) do
    :gen_statem.start_link({:local, service_name}, __MODULE__, {service_name, app_name, global_node_addr}, [])
  end

  @doc false
  def stop(name), do: :gen_statem.stop(name)

  @doc false
  @impl :gen_statem
  def init({service_name, app_name, global_node_addr}) do
    init_params = :rpc.call(global_node_addr, GlobalNode.ServicesSupervisor, :value, [service_name, :init_params])
    node_addr =
      try do
        machine_addr = Map.get(init_params, :machine_addr) || Map.get(init_params, "machine_addr")
        service = Map.get(init_params, :service) || Map.get(init_params, "service")
        :"#{service}@#{machine_addr}"
      rescue
        _e -> nil
      end

    {:ok, :connected, %{service_name: service_name, app_name: app_name, global_node_addr: global_node_addr, node_addr: node_addr}}
  end

  @doc false
  @impl :gen_statem
  def callback_mode, do: :handle_event_function

  @doc """
  Final implementation of event handling.

  FSM has 3 states -
    * connected - sends data via usual connection
    * disconnected - stores important functions to file
    * sending - works as connected but sends stored functions
  """
  @impl :gen_statem
  def handle_event({:call, from}, {:unhandled, func_type, params}, :connected, params = %{service_name: service_name, app_name: app_name}) do
    Utils.write_file(func_type, params, service_name, app_name)

    {:next_state, :disconnected, params,
     [
       {:state_timeout, @retry_timeout, :retry_connect},
       {:reply, from, :error}
     ]}
  end
  def handle_event({:call, from}, {:unhandled, func_type, params}, :sending, params = %{service_name: service_name, app_name: app_name}) do
    Utils.write_file(func_type, params, service_name, app_name)

    {:next_state, :disconnected, params,
     [
       {:state_timeout, @retry_timeout, :retry_connect},
       {:reply, from, :error}
     ]}
  end

  def handle_event({:call, from}, {:unhandled, func_type, params}, :disconnected, params = %{service_name: service_name, app_name: app_name}) do
    Utils.write_file(func_type, params, service_name, app_name)

    {:keep_state, params, [{:reply, from, :ok}]}
  end

  def handle_event(:state_timeout, :retry_connect, :disconnected, %{service_name: service_name, app_name: app_name, node_addr: node_addr, global_node_addr: global_node_addr}) do
    with  true <- try_connect(node_addr) || try_connect(with_global_params(global_node_addr, service_name)),
          true <- :global.whereis_name(service_name) |> is_pid do

      spawn(fn ->
        Utils.get_munin_module(service_name, app_name)
        |> apply(:list_logs, [])
        |> Utils.do_send(service_name, app_name)

        :gen_statem.call(service_name, :sended)
      end)

      {:next_state, :sending, %{service_name: service_name, app_name: app_name, global_node_addr: global_node_addr}, [{:state_timeout, @send_timeout, :do_send}]}
    else
      _e ->
        {:keep_state, %{service_name: service_name, app_name: app_name, global_node_addr: global_node_addr}, [{:state_timeout, @retry_timeout, :retry_connect}]}
    end
  end

  def handle_event({:call, from}, {:call, params}, :disconnected, params = %{service_name: service_name, app_name: app_name}) do
    {:unhandled_error, err} = Utils.do_error(service_name, "unavaliable service", params, "call")
    Utils.write_file("call", err, service_name, app_name)
    {:keep_state, params, [{:reply, from, err}]}
  end

  def handle_event({:call, from}, {:call, params}, _state, params = %{service_name: service_name, app_name: app_name}) do
    case Utils.do_call(service_name, params) do
      {:unhandled_error, err} ->
        Utils.write_file("call", err, service_name, app_name)

        {:next_state, :disconnected, params,
         [
           {:state_timeout, @retry_timeout, :retry_connect},
           {:reply, from, err}
         ]}

      any ->
        {:keep_state, params, [{:reply, from, any}]}
    end
  end

  def handle_event({:call, from}, :sended, :sending, params),
    do: {:next_state, :connected, params, [{:reply, from, :ok}]}

  def handle_event({:call, from}, :get_params, _content, params),
    do: {:keep_state, params, [{:reply, from, params}]}

  def handle_event({:call, from}, {:set_params, params}, _content, the_old_ones),
    do: {:keep_state, params, [{:reply, from, %{params: params, old_params: the_old_ones}}]}

  def handle_event({:call, from}, :list_log_files, _content, params  = %{service_name: service_name, app_name: app_name}),
    do:
      {:keep_state, params,
       [{:reply, from, apply(Utils.get_munin_module(service_name, app_name), :list_logs, [])}]}

  def handle_event({:call, from}, {:cast, params}, :disconnected, params  = %{service_name: service_name, app_name: app_name}) do
    {:unhandled_error, err} = Utils.do_error(service_name, "unavaliable service", params, "cast")
    Utils.write_file("cast", err, service_name, app_name)
    {:keep_state, params, [{:reply, from, err}]}
  end

  def handle_event({:call, from}, {:cast, params}, _state, params  = %{service_name: service_name, app_name: app_name}) do
    case Utils.do_cast(service_name, params) do
      {:unhandled_error, err} ->
        Utils.write_file("cast", err, service_name, app_name)

        {:next_state, :disconnected, params,
        [
          {:state_timeout, @retry_timeout, :retry_connect},
          {:reply, from, err}
        ]}

      any ->
        {:keep_state, params, [{:reply, from, any}]}
    end
  end

  def handle_event({:call, from}, _event, _content, state),
    do: {:keep_state, state, [{:reply, from, {:error, "invalid transition"}}]}

  def handle_event(_any, _event, _content, state), do: {:keep_state, state, []}

  def with_global_params(global_node_addr, service_name) do
    init_params = :rpc.call(global_node_addr, GlobalNode.ServicesSupervisor, :value, [service_name, :init_params])

    try do
      machine_addr = Map.get(init_params, :machine_addr) || Map.get(init_params, "machine_addr")
      service = Map.get(init_params, :service) || Map.get(init_params, "service")
      :"#{service}@#{machine_addr}"
    rescue
      _e -> nil
    end
  end

  def try_connect(nil), do: false
  def try_connect(addr) do
    Node.connect(addr)
  rescue
    _e -> false
  end
end
