defmodule Munin.Service.Global do
  @moduledoc """
  This service is about connecting to key value store with services info
  Attrs:
  * global_node_addr - node name (eg. :"global@172.25.78.153")
  * service - any name (eg. "thor")

  Usage:

      defmodule MyApp.GStore do
        use Munin.Service.Global,
          global_node_addr: :"global@172.25.78.153",
          service: "my_app"
      end

  and add to the application to init GStore task

      cheldrens: [
        ...,
        MyApp.GStore
        ...
      ]

  # then in any module of app

      iex> alias MyApp.GStore
      iex> GStore.save(:hello, "world")
      :ok
      iex> GStore.get(:hello)
      "world"
      iex> GStore.remove(:hello)
      :ok

  """
  @type module_vars :: [service: String.t(), params: map()]

  @spec __using__(module_vars) :: {:__block__, [], list()}
  defmacro __using__(service: service, params: params) when is_binary(service) do
    quote do
      use Agent

      @spec start_link(any) :: {:ok, pid}
      def start_link(init_val) do
        :io.format("starting service: #{unquote(service)}\r", [])
        init_params =
          %{machine_addr: Munin.fetch_machine_addr()}
          |> Map.merge(Enum.into(init_val, %{}))
          |> Map.merge(unquote(params))
          |> Map.merge(%{
              datetime_started: NaiveDateTime.local_now(),
              global_addr: Keyword.get(init_val, :global_node_addr, :"global@172.25.78.153"),
              service: unquote(service)
          })
        save(:init_params, init_params)
        :io.format("global communication info sended\n", [])
        :io.format("~n", [])

        Agent.start_link(fn -> init_params end, name: __MODULE__)
      end

      def get_global, do: Agent.get(__MODULE__, & &1) |> Map.get(:global_node_addr, :"global@172.25.78.153")

      def save(map) when is_map(map) do
        Enum.each(map, fn {k, v} -> save(key, value) end)
      end

      def save(key, value) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :store, [
          unquote(service),
          key,
          value
        ])
      end

      def get(service, key) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :value, [unquote(service), key])
      end

      def remove(service, key) do
        :rpc.call(get_global(), GlobalNode.ServicesSupervisor, :remove, [unquote(service), key])
      end
    end
  end
end
